package io.github.hajto.cohhcarnagegiveawayclient.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.github.hajto.cohhcarnagegiveawayclient.R;

public class NoGiveAwayFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.no_giveaway, container, false);
    }
}
