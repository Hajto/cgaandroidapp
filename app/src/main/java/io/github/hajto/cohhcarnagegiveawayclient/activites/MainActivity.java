package io.github.hajto.cohhcarnagegiveawayclient.activites;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Set;

import io.github.hajto.cohhcarnagegiveawayclient.R;
import io.github.hajto.cohhcarnagegiveawayclient.constants.Logging;
import io.github.hajto.cohhcarnagegiveawayclient.model.GiveAway;
import io.github.hajto.cohhcarnagegiveawayclient.view.Action;
import io.github.hajto.cohhcarnagegiveawayclient.view.GiveAwayFragment;
import io.github.hajto.cohhcarnagegiveawayclient.view.NoGiveAwayFragment;

public class MainActivity extends AppCompatActivity {

    GiveAway giveAway;

    public MainActivity() {
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getExtras() != null) {
            giveAway = GiveAway.extractFromBundle(getIntent().getExtras());
        }

        if (giveAway != null && giveAway.isValid())
            showGiveAway(giveAway);
        else
            hideGiveAway();
    }

    @Subscribe
    public void onMessageEvent(final GiveAway event) {
        giveAway = event;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showGiveAway(event);
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        GiveAway saved = (GiveAway) savedInstanceState.getSerializable("giveaway");
        if (saved != null) {
            giveAway = saved;
            showGiveAway(saved);
        }
    }

    private void showGiveAway(GiveAway saved) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("giveaway", saved);
        GiveAwayFragment fragment = new GiveAwayFragment(new Action() {
            @Override
            public View.OnClickListener onPositive() {
                return null;
            }

            @Override
            public View.OnClickListener onNegative() {
                return new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(Logging.GIVEAWAY_FRAGMENT, "Negative button clicked");
                        hideGiveAway();
                    }
                };
            }
        });
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commitAllowingStateLoss();
    }

    private void hideGiveAway() {
        NoGiveAwayFragment noGiveAwayFragment = new NoGiveAwayFragment();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, noGiveAwayFragment)
                .commitAllowingStateLoss();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (giveAway != null) {
            outState.putSerializable("giveaway", giveAway);
        }
        Log.i("savedState", outState.toString());
        super.onSaveInstanceState(outState);
    }
}
