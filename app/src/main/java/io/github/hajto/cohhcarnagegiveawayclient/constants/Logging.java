package io.github.hajto.cohhcarnagegiveawayclient.constants;

public class Logging {
    public static final String GIVEAWAY_FRAGMENT = "GiveAwayFragment";
    public static final String GIVEAWAY = "GiveAwayModel";
}
