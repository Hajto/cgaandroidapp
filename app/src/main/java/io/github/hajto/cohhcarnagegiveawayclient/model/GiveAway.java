package io.github.hajto.cohhcarnagegiveawayclient.model;

import android.os.Bundle;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.Serializable;
import java.util.Date;

import io.github.hajto.cohhcarnagegiveawayclient.constants.Logging;

public class GiveAway implements Serializable {
    public static final int STREAM_DELAY = 30;
    private String game;
    private String donor;
    private String prize;
    private Date start;

    public GiveAway(String game, String donor, String prize, String start) {
        this(game, donor, prize, Long.parseLong(start));
    }

    public GiveAway(String game, String donor, String prize, long start) {
        this.game = game;
        this.donor = donor;
        this.prize = prize;
        this.start = new Date(start);
    }

    public String getGame() {
        return game;
    }

    public String getDonor() {
        return donor;
    }

    public String getPrize() {
        return prize;
    }

    public Date getStart() {
        return start;
    }

    public boolean isValid() {
        return remainingMilis() > STREAM_DELAY;
    }

    public DateTime getRemainingTime() {
        DateTime remainingTime = new DateTime(remainingMilis());
        remainingTime = remainingTime.withZoneRetainFields(DateTimeZone.UTC);

        return remainingTime;
    }

    public long remainingMilis() {
        long starting = start.getTime();
        long now = new Date().getTime();
        long ending = starting + 3 * 60 * 1000;
        return ending - now;
    }

    @Override
    public String toString() {
        return donor + " is giving out " + game + " for " + prize + " tokens";
    }

    public static GiveAway extractFromBundle(Bundle bundle) {
        String game = (String) bundle.get("game");
        String donor = (String) bundle.get("donor");
        String prize = (String) bundle.get("tokens");
        Long time = (Long) bundle.get("google.sent_time");
        if (time != null)
            return new GiveAway(game, donor, prize, time);
        else
            return null;
    }
}
