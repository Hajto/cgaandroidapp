package io.github.hajto.cohhcarnagegiveawayclient.view;

import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import io.github.hajto.cohhcarnagegiveawayclient.R;
import io.github.hajto.cohhcarnagegiveawayclient.model.GiveAway;

public class GiveAwayFragment extends Fragment {

    private TextView game;
    private TextView details;
    private Action action;

    public GiveAwayFragment(Action action) {
        this.action = action;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.awaiting_giveaway_fragment, container, false);

        game = (TextView) root.findViewById(R.id.giveaway_game);
        details = (TextView) root.findViewById(R.id.giveaway_details);

        Bundle data = getArguments();
        final GiveAway giveAway = (GiveAway) data.getSerializable("giveaway");

        assert giveAway != null;
        game.setText(giveAway.getGame());
        details.setText("Sponsored by " + giveAway.getDonor() + " and it's worth "
                + giveAway.getPrize() + " tokens!");

        final RoundCornerProgressBar bar = (RoundCornerProgressBar) root.findViewById(R.id.progress_bar);

        new CountDownTimer(giveAway.remainingMilis(), 1000/60) {

            public void onTick(long millisUntilFinished) {
                bar.setProgress(180000 - giveAway.remainingMilis());
            }

            public void onFinish() {
                details.setText("GiveAway has finisehd");
            }
        }.start();

        root.findViewById(R.id.negative_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("GiveAwayFragmetn", "Negative clicked");
                action.onNegative();
            }
        });

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
