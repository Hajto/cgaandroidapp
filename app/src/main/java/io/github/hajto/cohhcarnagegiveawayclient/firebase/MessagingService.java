package io.github.hajto.cohhcarnagegiveawayclient.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import io.github.hajto.cohhcarnagegiveawayclient.model.GiveAway;

public class MessagingService extends FirebaseMessagingService {

    public MessagingService() {
        Log.d("Messages", "Luanchging service");
//        FirebaseMessaging.getInstance().subscribeToTopic("giveaways");
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        GiveAway giveAway = new GiveAway(data.get("game"), data.get("donor"), data.get("tokens"), remoteMessage.getSentTime());
        if (giveAway.isValid()) {
            EventBus.getDefault().postSticky(giveAway);
        }
    }
}
