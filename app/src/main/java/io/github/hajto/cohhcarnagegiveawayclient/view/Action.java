package io.github.hajto.cohhcarnagegiveawayclient.view;

import android.view.View;

public interface Action {
    public View.OnClickListener onPositive();
    public View.OnClickListener onNegative();
}
